use std::io::{BufReader, BufRead, self};
use std::fs::{File};
use std::path::Path;
use math::round;

pub fn task_one(file_path: String) -> i32 {

    // get file contents
    let mut total_mass: i32 = 0;
    if let Ok(lines) = read_lines(file_path) {
        for line in lines {
            if let Ok(mass_string) = line {
                let setting=mass_string.parse::<i32>().unwrap(); // don't care about errors
                let mass: i32 = calculate_fuel(setting);
                total_mass = total_mass + mass;
            }
        }
    }

    return total_mass;
}

pub fn task_two(file_path: String) -> i32 {
    let mut total_fuel: i32 = 0;
    if let Ok(lines) = read_lines(file_path) {
        for line in lines {
            if let Ok(mass_string) = line {
                let setting=mass_string.parse::<i32>().unwrap(); // don't care about errors

                let fuel=get_fuel_needed(setting);
                total_fuel += fuel;
            }
        }
    }

    return total_fuel;
}

fn get_fuel_needed(setting: i32) -> i32 {
    let mut fuel: i32 = calculate_fuel(setting);
    let mut total_fuel = fuel;

    while fuel > 0 {
        fuel = calculate_fuel(fuel);
        if fuel >0 {
            total_fuel += fuel;
        }
    }
    return total_fuel;
}

pub fn calculate_fuel(curr_mass: i32) -> i32 {
    // divide by 3
    // round down
    // subtract 2
    let mut running: f64 = f64::from(curr_mass) / 3.0;
    running = round::floor(running, 0);
    running = running - 2.0;

    return running.round() as i32;
}

fn read_lines<P>(filename: P) -> io::Result<io::Lines<io::BufReader<File>>> where P: AsRef<Path>, {
    let file = File::open(filename)?;
    Ok(BufReader::new(file).lines())
}