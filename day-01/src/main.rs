use day_01::{task_one, task_two};

fn main() {
    println!("Day 1 - Task 1");
    println!("{}", task_one("input_1.txt".to_owned()));

    println!("Day 1 - Task 2");
    println!("{}", task_two("input_1.txt".to_owned()));
}
