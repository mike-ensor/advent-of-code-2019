#[cfg(test)]
mod test {
    use day_01::{task_one, task_two, calculate_fuel};


    #[test]
    fn test_task_one() {
        let result = task_one("input_1_test.txt".to_owned());

        assert_eq!(result, 34241, "Result does not match expected output");
    }

    #[test]
    fn test_task_two() {
        let result = task_two("input_1_test.txt".to_owned());

        assert_eq!(result, 51316, "Result does not match expected output");
    }

    #[test]
    fn test_calculate_mass() {
        let result = calculate_fuel(12);
        assert_eq!(result, 2);

        let result = calculate_fuel(100756);
        assert_eq!(result, 33583);
    }
}

